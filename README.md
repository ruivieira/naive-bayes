![https://crates.io/crates/naivebayes](https://img.shields.io/crates/v/naivebayes.svg) ![https://docs.rs/naivebayes/](https://docs.rs/naivebayes/badge.svg) ![](https://gitlab.com/ruivieira/naive-bayes/badges/master/build.svg
)
# naive-bayes

A [Naive Bayes](https://en.wikipedia.org/wiki/Naive_Bayes_classifier) classifier written in Rust.

## installation

Add to your `Cargo.toml`:

```text
naivebayes = "0.1.1"
```

## usage

Add the crate and `NaiveBayes` to your code:

```rust
extern crate naivebayes;

use naivebayes::NaiveBayes;
```

Initialise the classifier and train it classifier by passing `Vec<String>` of tokens, along with a label:

```rust
let mut nb = NaiveBayes::new();
nb.train(&tokens, &label);
```

Use another set of tokens as `Vec<String>` to classify it:

```rust
let classification = nb.classify(&tokens_classify);
print!("classification = {:?}", classification);
```

Alternitavely, to prevent a potential calculation underflow with very small probabilities, the `log_classify`
method can be used:

```rust
let classification = nb.log_classify(&tokens_classify);
print!("classification = {:?}", classification);
```